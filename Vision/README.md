# Vision

## Practical part

### Use case

This web application aims to help sorting waste. Based on the waste image it will decide which trash box it should be thrown in.

![Image](images/app.png)

### Demo video

https://youtu.be/vQHmiGHB17g

### Dataset

* Glass/paper/cardboard/plastic/metal/trash: https://github.com/garythung/trashnet - each class contains 400-600 of images

I will use the above dataset and merge some classes: paper + cardboard into just 'Paper' and metal and plastic into
'Metal and Plastic' - these are the categories used in Poland.

* In addition, I added one class of waste: Bio (Organic). I picked 1k best images from class 'O' from kaggle dataset: 
https://www.kaggle.com/techsash/waste-classification-data
 
### Architecture

![Image](images/architecture.png)

Users will have an access to application via Web App. Web App is built with Node and React. It sends POST requests with
provided images to Custom Vision API. The model for deciding which waste category is the trash is implemented in Custom Vision.


### Steps

* create resource. Go to https://www.customvision.ai/ and click 'New project'. Then you have to create a Cognitive
Services resource and a resource group. I chose 'classification', 'multiclass' and 'general' options.

* add every class of images using 'Add images' option. For every class provide a tag name, e.g.:

![Image](images/add.png)

* click 'Train' and choose 'quick' or 'advanced' training. I chose quick, because advanced costs 20$ per training hour.
After 10 minutes I got the result:

![Image](images/train.png)

* after clicking 'Quick test' you can upload some images and try the model, sample images (outside the dataset):

![Image](images/test1.png)
![Image](images/test2.png)

* click 'Publish' from Performance tab and choose resource group. Now the model is published and can be accessed via
Prediction API. Select 'Prediction URL' button to see the details:

![Image](images/api.png)

* download Azure App Service extension for VS code and login to Azure

* create an UI for application (I used npx create-react-app for the template)

* click 'Publish' and 'Yes' to all prompts

* when using React and changing the code the parameter SCM_DO_BUILD_DURING_DEPLOYMENT in '.deployment' file must be set to false,
otherwise publishing will return errors

* app can be accessed by clicking 'Browse'

Result:

![Image](images/app2.png)


### Resources
* https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/getting-started-build-a-classifier
* https://docs.microsoft.com/en-us/azure/cognitive-services/custom-vision-service/use-prediction-api
* https://github.com/garythung/trashnet
* https://www.kaggle.com/techsash/waste-classification-data

-----------------------------------
## Theory part

### Computer Vision API 
#### Intro

* Face API

    - API for detecting and analyzing faces. 
    - 5 categories of tasks: verification, detection, identification, similarity, grouping.
    - returns locations of the face, landmarks (position of common face elements), attributes 
    like age, gender, hair color, emotion.


* Emotion API
    
    - API for assigning the level of confidence on detecting emotions:
      anger, contempt, disgust, fear, happiness, neutral, sadness, surprise.
    - it adds a collection of emotions to Face API payload
    
plus other, like e.g. OCR.
    
#### Use cases
* Analyze people emotions to get information about the impact of some products/places on people
* Detect people on CCTV when the area has restricted access
* Get car numbers from speed camera photos

#### How to
To use API we just have to create a proper resource and get the key with endpoint. It's available
under Resource Management -> Keys and Endpoints. Connecting with REST can be simplified by using
MS libraries, like azure-cognitiveservices-vision for Python.

#### Pricing
For vast majority of features the pricing is as follows:
* first 1000 units/month - free
* 1001 - 5 mln/month - $1,50
* 5 - 20 mln/month - $0,60


### Custom Vision
#### Intro
Custom Vision enables creating custom image classification models, learned from pairs of images and their labels
provided. Then, the model results can be accessed as an API.

#### Use cases
* application for medical doctors which returns the probability of illness based on rtg photos
* camera system in waste sorting plant, which decides where the trash should go (plastic/metal/glass and so on)
* automatically labeling system in online store, which predicts the type and its attributes based on the clothes 
photos

#### How to
To build a custom model you need to go to Custom Vision Service portal and click 'New project' there.
You can choose your project type (e.g. classification), classification types (e.g. multilabel) and domain.
Then, add tags with images to the project. You can train the model, see results and their measures of accuracy, 
and test it. After training our model we can publish it and get an URL to Prediction API endpoint.  

#### Pricing
* 2 transactions per second, max 2 projects, up to 1 hour of model training per month - free
* 10 transactions per second, max 100 projects - $2 per 1000 transactions + $20 per 1 hour of training + $0.70
per 1000 images in datastore


### Video Indexer
#### Intro
Service used for uploading and indexing videos from multiple types of sources and format. Video Indexer
is able to extract some insights from the video files and edit them.

#### Use cases
* system for police which can find some special moments in video recordings, e.g. looking for some particular 
people after committing a crime
* modifying videos to cut off some elements from background
* application which counts how many people bought products in respect to how many people entered the store

#### How to
Sign in into Video Indexer Portal and get your own API key ('Products' -> 'Authorization'). The endpoint accepts
videos from url or from local files and returns an object with detailed information about the video, the most
important are insights, which include: transcripts, OCR, keywords, labels, faces, brand, sentiments and emotions.

#### Pricing
* video analysis - $0.15 per input minute
* audio analysis - $0.04 per input minute
* basic audio analysis - $0.02003 per input minute