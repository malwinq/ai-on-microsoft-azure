import logo from './logo.svg';
import './App.css';
import ImageUpload from './ImageUpload';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Select an image and check which bin it should be thrown in !
        </p>
        <ImageUpload/>
      </header>
    </div>
  );
}

export default App;
