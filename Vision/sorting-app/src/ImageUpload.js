import React from 'react';
import './ImageUpload.css';
import axios from 'axios';
import ClipLoader from "react-spinners/ClipLoader";

class ImageUpload extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        file: '',
        imagePreviewUrl: '',
        bin: null,
        prop: -1  
      };
    }
  
    _handleSubmit(e) {
      this.setState({
        prop: null,
        bin: null
      })
      e.preventDefault();
      axios.post("https://southcentralus.api.cognitive.microsoft.com/customvision/v3.0/Prediction/c5571a6d-c1b3-4846-8aff-5667e651caca/classify/iterations/sorting-model-1/image", 
        this.state.file, {
        headers: {
          "Content-Type": "application/octet-stream",
          "Prediction-Key": "36f86d61b7614703b837c5b6c3b47dc5"
        }
      }).then(res => {
        const pred = res.data.predictions;
        let max_score=0;
        let result;
        pred.forEach(item => {
          if (item.probability > max_score) {
            max_score = item.probability;
            result = item.tagName;
          }
        });
        this.setState({
          prop: Math.round(max_score * 100),
          bin: result
        });
      }).catch(err => {
        console.error({err});
      });
      console.log(this.state.prop, this.state.bin);
    }
  
    _handleImageChange(e) {
      e.preventDefault();
  
      let reader = new FileReader();
      let file = e.target.files[0];
  
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }
  
      reader.readAsDataURL(file)
    }
  
    render() {
      let { imagePreviewUrl, prop, bin } = this.state;
      let $imagePreview = null;
      let $result = null;
      if (imagePreviewUrl) {
        $imagePreview = (<img className="image" src={imagePreviewUrl} />);
      } else {
        $imagePreview = (<div className="previewText">Please select an image for preview</div>);
      }

      if (bin) {
        $result = (<div><div className="resultText">Result: {bin} </div><div className="propText">
        (PROBABILITY {prop}%)</div></div>);
      } else {
        if (prop < 0) {
          $result = null
        } else {
          $result = (<div className="loadingIcon">
            <ClipLoader
            size={40}
            color={"#8b22b4"}
          /></div>);
        }
      }
  
      return (
        <div className="previewComponent">
          <form onSubmit={(e)=>this._handleSubmit(e)}>
            <input className="fileInput" 
              type="file" 
              onChange={(e)=>this._handleImageChange(e)} />
            <span>    </span>
            <button className="submitButton" 
              type="submit" 
              onClick={(e)=>this._handleSubmit(e)}>UPLOAD IMAGE</button>
          </form>
          <div className="imgPreview">
            {$imagePreview}
            {$result}
          </div>
        </div>
      )
    }
  }

export default ImageUpload;