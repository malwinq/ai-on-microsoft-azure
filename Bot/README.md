# Cooking Bot 


## Practical part - Cooking bot

### Use case
The bot will be able to search for available recipes containing ingredients provided by the user, tell jokes
and even compose a diet plans. I will use a 'Recipe-Food-Nutricion API' (link to RapidAPI: 
https://rapidapi.com/spoonacular/api/recipe-food-nutrition/details).

### Demo video

https://youtu.be/8nFASuPaccc

### Used services
* Bot Framework Composer
* Bot Framework Emulator
* LUIS

### Dialogs
Bot has 5 possible dialogs implemented:
* getRecipes - asking for ingredients you want to use and choosing the meal
* getMealPlans - asking for the type of plan and number of goal calories, returns a daily meal plan
* getJoke - returns a food joke
* help
* cancel

### REST API
To get access to Recipe-Food-Nutrition API I created new account on RapidAPI website, then searched for the API and go
to 'Pricing' tab. I selected the Basic Plan, which is free and contains 500 requests per day.

![Image](images/api_pricing.png)

API url: https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/

Used endpoints (only GET method):
* /recipes/findByIngredients - search for recipe by ingredients provided in the query body, returns a id of the recipe
* /recipes/{id}/information - gets the link to recipe, description and list of ingredients 
* /recipes/mealplans/generate - generates daily plan of meals taking into account the type of diet (vegetarian, vegan)
with target calories and excluding provided ingredients
* /recipes/random - gets a random recipe
* /food/jokes/random - gets a random food joke (it's british humour, so...)

### Steps
1. Install Bot Framework Composer
2. Create new bot and first action
3. Create dialogs
4. Add HTTP requests
5. Connect triggers with dialogs
6. Add LUIS 

### Prerequirements
* yarn
* .NET SDK v3.1
* Bot Framework Emulator

### Install Bot Composer

To install Bot Composer I used the github repo. Terminal commands:

```git clone https://github.com/microsoft/BotFramework-Composer.git```

```cd BotFramework-Composer/Composer```

```yarn```

```yarn build```

```yarn startall```

Bot Composer is running on http://localhost:3000 

### Creating a simple bot

In Bot Framework Composer click 'New' and 'Create from scratch'. Enter the name, description and folder.
New bot has the default trigger called Greeting. I changed the name to 'GreetTheUser' and added new action ('Send
the response' by clicking the plus button), then entered the welcoming message on the right panel.
After deleting unnecessary stuff it looks like this:

![Image](images/actions_1.png)

Click 'Start the bot' and 'Test in Emulator'

### Add dialogs

Adding new dialogs are done by clicking 'Add' -> 'Add new dialog'.
First dialog here is 'getRecipe', which will be responsible for
searching for available recipes with provided ingredients. The first
trigger is added by default (BeginTrigger).

To connect the dialogs with main bot choose 'Regular expression 
recognizer' from Language Understanding properties (main panel).
Then select 'Add new trigger' and choose 'Intent recognized' in the
field type, enter the regex for trigger as following:

![Image](images/trigger_1.png)

It tells the bot to look for the word 'recipe' in user messages.
Then, click plus button in created area and select 'Dialog
management' -> 'Begin a new dialog'. In panel on right side choose 
'getRecipe' action.

To get the ingredients from user go into 'getRecipe' and add new 
'Ask new question' -> 'Text'. In 'Bot Asks' create a question, then
'User Input' defines the answer:
* property - name of variable
* output format - expression how the value will be saved (like
trim(this.value))

In 'Other' card it's possible to set:
* unrecognized prompt - message sent to user if the value is wrong
* validation - how the value will be validated (e.g. the length
of input and input type). For ingredients I used:
```(this.value.match(/\d+/g) == null)```
* prompt values - specify the default value

### Add the HTTP requests

In the dialog canvas, click plus button after the last message
and select 'Access external properties' -> 'Send an HTTP request'.
Enter proper values to url, body and other parameters. For recipe searching:

![Image](images/http.png)

(code snippets of how to use this API are here: https://rapidapi.com/spoonacular/api/recipe-food-nutrition?endpoint=55e1b3e1e4b0b74f06703be6)

Below the http request I made a if/else branch for catching the errors. 

After implementing all of the branches, loops and queries the flow for 
getRecipe looks like follows:

![Image](images/recipe_flow.png)

The rest of paths were added in the same way. 

Flow for jokes:

![Image](images/joke_flow.png)

Flow for meal plans:

![Image](images/plan_flow.png)

Canceling:

![Image](images/cancel_flow.png)

Helping:

![Image](images/help_flow.png)

### Add LUIS

To add LUIS triggers choose 'Default recognizer' in the main panel on the right side, then 'Add'
-> 'Create a trigger'. Enter the name and in 'Trigger phrases' provide some phrases, like for canceling
'stop', 'cancel', 'end', , 'bye', 'please end' and so on. I added LUIS to all triggers.

When clicking 'Restart Bot' you will be prompted for LUIS key. Go to LUIS site, log in and create new LUIS
resource under the CookingBot.

! The error occurs when using Azure Pass: MissingSubscriptionRegistration:: service Microsoft.CognitiveServices is not registered under subscription.
I needed to go to Azure Portal -> click my subscription -> go to 'Settings' -> 'Resource providers', then
search for Microsoft.CognitiveServices and click 'Register'.

The LUIS key can be found under account icon -> Manage resources -> Authoring resource -> Primary key.



-----------------------------------

## Theory part

### Text Moderation API 

#### Intro
API responsible for finding information in text like:
*  personally identifiable information (PII)
*  list of unwanted words
*  types of unwanted words

The response from Text Moderation API is a JSON with fields:
* Terms - profanity words with its indexes
* Classification - classify offensive words into 3 categories and let us know if the text should be manually checked
* PII - personal information found in the text, like: email, IP address, phone number

#### Use cases
* automated checking if public comments are offensive and block them
* secure system in a company checking if employees send sensitive data outside the company
* police system for finding pedophiles on websites for children

#### How to
1. Create Content Moderator resource
2. You can provide your query params (autocorrect, PII, listId, language) for test
3. Choose text/plain in 'Content-Type' field
4. Provide sample text and evaluate the response

Pricing: no upfront cont, 'pay only what you use'
* to 1 transaction per second - free
* 10 transactions per second: from $1 (0-1M transactions) to $0.40 (10M+ transactions) per 1,000 transactions


### Language Understanding Intelligent Service (LUIS)

#### Intro
Using LUIS aims to build bot's conversational intelligence. The service uses machine learning 
for extraction of text features which will help pulling out relevant information without reading
all text. LUIS can tell us:
* what is the intent of the speech, unified goal of whole text
* what are the entities the person used which caused choosing that particular intent and what
are the parameters of this intent

#### Use cases
* 1st step while connecting with helpdesk - could segregate the issues and send them to 
proper consultants
* hotel service bot, which understands what clients want to get e.g. from restaurant or 
 a room cleaning
* service for posting information about road traffic, users could send short messages 
with info where and what is causing the blockage and LUIS could parse these messages into
nice form of issue table, so that everyone can quickly search for issues they're interested in

#### How to
Creating the app:
1. Create Language Understanding resource
2. Go to one of URLs of LUIS application, for Europe: https://eu.luis.ai/
3. Sign in with Microsoft account
4. Click 'Create a LUIS app now' -> '+ Create a new app'
5. Name your app and select culture
6. Add a description of your app, then click 'Done' and 'Build'

Adding intents:
1. Click '+ Create new intent'
2. Name the intent and click 'Done'
3. Enter intent utterances (should be always at least 5)

Creating entities:
1. Select 'Entities', click '+ Create' and name the entity
2. Select 'Machine Learned' for 'Entity type', then 'Create'
3. Map facet entities with intents

Select 'Train' in the top bar

Connect LUIS app with Azure Resource
1. Select 'Manage' in the toolbar -> 'Azure Resource'
2. You should see 'Prediction Resource' with the name of endpoint
3. Select subscription and click 'Done'
4. Select 'Publish' in the toolbar -> 'Production Slot'
5. You can test the app with 'Test' panel

Pricing:
* Free - 5 transactions per second - 10,000 transactions per month (but text only)
* Standard - 50 trans/sec - $1.50 text or $5.50 speech requests per 1,000 transactions


### Text Analytics API

#### Intro
This service is built for analyzing a text and extracting e.g. sentiment analysis, 
key phrases, information about language and so on. Text Analytics API uses ML for scoring the 
sentiment (values between 0 - negative and 1 - positive). The service can be used via 
endpoints, so users can just sent a post request to get the text analysis as the response.

#### Use cases
* an app comparing products from different companies based on users' opinions
* website for scoring the movies based on opinions
* making statistics of opinions and key disadvantages pointed by the clients on a website

#### How to
1. Create Text Analytics resource
2. Copy the value of one key from 'Keys' -> 'Menage keys'
3. Go to the website https://[location].dev.cognitive.microsoft.com/docs/services/TextAnalytics.V2.0
where [location] is e.g. 'eastus'.
4. You can choose the option from: 'Detect Language', 'Entities', 'Key Phrases', 'Sentiment', 
then add an access key to the call
5. The requests can be sent from 'API Testing Console', where you type the parameters of
requests

Since Text Analytics API can be wrapped in Azure Function, there are a lot
of ways how to make an application with this service.

Pricing:

* Free - 5,000 transactions per month
* Standard - from $2 - up to 500,000 to $0.25 - 10M+ text records per
1,000 text records
* S0 - S1 - from $74,71 (up to 25,000 transactions) to $4,999 (up to 10,000,000 transactions)
per month

-----------------------------------

### QnA Maker

#### Intro
QnA Maker enables creating and publishing knowledge base for
bots. It consists of questions and answers pairs, which can be generated
from existing FAQs or entered manually. The service analyzes pairs
and applies NLP model, which can be accessed later via REST interface.

#### Use cases
* create the base of QnA from large, existing FAQ documents without
spending much time, so useful for any big web services, like online shops
* create a knowledge base from the history of previously used bots or
conversations

#### How to
1. Go to QnA Maker portal at https://qnamaker.ai and sign in.
2. Select 'Create a knowledge base' and 'Create na QnA service'
3. In Step 4, select the faq document (e.g. from github repo)
4. Click 'Create your KB'
5. You can edit the knowledge base by options 'Add QnA pair' or 'Add alternative phrasing'
6. Click 'Save and train', optionally you can test it
7. Click 'Publish'

Pricing: 
* free - up to 3 transactions per second, up to 50k transactions per month, manage of 3 documents per month
* standard - up to 3 TPS, $10 for any number of documents

### Azure Bot Service

#### Intro
It's a framework for developing, publishing and managing bots.
Within this service it's possible to control conversation flow and
integrate with QnA Maker, but also to extend bot's functionality, 
test the bot and integrate with other services.

#### Use cases
Any bots applications, e.g.:
* online shops
* helpdesks
* info centers
* travel agencies
* hotels (booking)
* banks (loan consultants)

#### How to
1. Create Machine Learning resource
2. Launch Azure Machine Learning studio and sign into the service
3. Create new Compute Instance (Manage -> Compute)
4. Create new notebook (Author -> Notebooks)
5. Clone the github repo https://github.com/MicrosoftDocs/ai-fundamentals
6. Run the cells
7. Open 03a - QnA_Bot.ipnyb notebook from ai-fundamentals folder and run its cells
8. Create QnA knowledge base (up)
9. Click 'Create Bot'
10. Navigate to new tab where you can create a Web App Bot
11. After creating the bot click 'Go to resource' 
12. Click 'Test in Web Chat' and test the bot

Pricing:
* free - standard channels: without limitations, premium channels: up to 10k per month
* S1 - standard channels: without limitations, premium channels: $0.50 for 1000 messages

Standard channels are the Microsoft services and services with publicly available Bot APIs,
like Skype, Cortana, Teams, Facebook, Slack

Premium channels allow the bot to communicate with custom websites

-----------------------------------

### Azure Cognitive Speech Services
#### Intro
Speech service is used for transcribing audio streams into text. 
The service can be accessed via SDK client library or REST API.
It includes the functionality of translating to and from multiple
languages by machine translation. Speech Service also enables application
to generate the output as spoken version of input text.

#### Use cases
* audio home assistants 
* translators between languages from both text and speech
* GPS and car systems (speaking while driving)
* converting e-books to audio books
* assistant for blind people

#### How to
1. Create the Speech resource
2. To access the Speech Service you need to know the endpoints, available
under the RESOURCE MANAGERMENT group, select 'Keys and Endpoint' to display Key 
and Endpoint values. Copy KEY 1 and KEY 2.

##### Speech-to-text
1. Add the audio file
2. Create the code files with instructions: https://docs.microsoft.com/en-us/learn/modules/transcribe-speech-input-text/5-exercise-convert-speech-from-audio-file?pivots=python
3. Get the result of translation by recognize_once() function 

##### Text-to-speech
1. Create texttospeech file with instructions: https://docs.microsoft.com/pl-pl/learn/modules/synthesize-text-input-speech/3-synthesize-speech-audio-output?pivots=python
2. You can choose a voice from male and female in different languages or use a neural voice
3. Click 'Run File in Terminal' and hear the played audio or save audio file with generated speech

##### Speech-to-speech
1. Create objects: SpeechTranslationConfig, TranslationRecognizer,
TranslationRecognizerResult, SpeechSynthesizer from instructions: https://docs.microsoft.com/pl-pl/learn/modules/translate-speech-speech-service/3-translate-speech-different-language?pivots=python
2. Click the prompt 'Say something...' and speak a phrase

Pricing:
* free:
    * speech-to-text: standard - 5 audio hours per month, custom - 5 audio hours per month
    and 1 model endpoint hosting per month
    * text-to-speech: standard - 5 million characters per month, 
    neural - 0.5 million characters per month, custom - 5 million characters
    per month and 1 model endpoint hosting
    * speech translation: standard - 5 audio hours per month
* standard:
    * speech-to-text: standard - $1 per audio hour, custom - $1.40 per audio hour + $1.30 
    per endpoint hosting model, multi-channel - $2.10 per audio hour
    * text-to-speech: standard - $4 per 1M chars, neural -$16 per 1M chars, 
    custom - $6 per 1M chars + $0.05 per model per hour
    * speech translation: standard: $2.50 per audio hour
