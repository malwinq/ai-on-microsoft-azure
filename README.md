# AI-on-Microsoft-Azure

Univeristy course 


## 1st project - Cooking Bot

Folder 'Bot'

### Use case
The bot is able to search for available recipes containing ingredients provided by the user, tell jokes
and even compose a diet plans. I used a 'Recipe-Food-Nutricion API' (link to RapidAPI: 
https://rapidapi.com/spoonacular/api/recipe-food-nutrition/details).

### Demo video

https://youtu.be/8nFASuPaccc

## 2nd project - Vision 

Folder 'Vision'

### Use case
It's a web application which aims to help sorting waste. Based on the waste image it will decide which trash box it should be thrown in.

### Demo video

https://youtu.be/vQHmiGHB17g
